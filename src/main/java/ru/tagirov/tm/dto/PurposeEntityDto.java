package ru.tagirov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tagirov.tm.enumeration.Status;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class PurposeEntityDto extends AbstractDto{

    @NonNull
    private String name;

    @NonNull
    private String description;

    @NonNull
    private Date dateBegin;

    @Nullable
    private Date dateEnd;

    @NotNull
    private String userId;

    @NotNull
    private Status status = Status.PLANNED;

    public PurposeEntityDto(@NotNull final String id,
                         @NotNull final String name,
                         @NotNull final String description,
                         @NotNull final Date dateBegin,
                         @NotNull final Date dateEnd,
                         @NotNull final String userId,
                         @NotNull final Status status) {
        super(id);
        this.name = name;
        this.description = description;
        this.dateBegin = dateBegin;
        this.dateEnd = dateEnd;
        this.userId = userId;
        this.status = status;
    }
}
