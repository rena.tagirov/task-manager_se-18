package ru.tagirov.tm.repository;

//public class TaskRepository implements ITaskRepository {
//
//    private EntityManager entityManager;
//
//    public void setEntityManager(@NotNull EntityManager entityManager) {
//        this.entityManager = entityManager;
//    }
//
//    //    CRUD ----------------------------------------------------------------------
//
//    @Override
//    public void persist(Task task) throws SQLException {
//        entityManager.persist(task);
//    }
//
//    @Override
//    public void merge(Task task) throws SQLException {
//        entityManager.merge(task);
//    }
//
//    @Override
//    public Task findOne(String uuid) throws SQLException {
//        return entityManager.find(Task.class, uuid);
//    }
//
//    @Override
//    public Collection<Task> findAll() throws SQLException {
//        return entityManager.createQuery("SELECT u FROM Task as u", Task.class).getResultList();
//    }
//
//    @Override
//    public void remove(@NotNull final Task task) throws SQLException {
//        @NotNull final Task newTask = entityManager.find(Task.class, task.getId());
//        entityManager.remove(newTask);
//    }
//
//    @Override
//    public void removeAll() throws SQLException {
//        entityManager.createQuery("DELETE FROM Task p", Task.class).executeUpdate();
//
//    }
//
//    //    ALL ------------------------------------------------------------------------
//
//    @Override
//    public @Nullable Task findOneByUserId(String userId, String uuid) throws SQLException {
//        return entityManager.createQuery("SELECT p FROM Task p WHERE p.user_id =: user_id AND p.id = : id", Task.class).
//                setParameter("user_id", userId)
//                .setParameter("id", uuid)
//                .getSingleResult();
//    }
//
//    @Override
//    public Collection<Task> findAllByUserId(String userId) throws SQLException {
//        return entityManager.createQuery(
//                "SELECT p FROM Task p WHERE p.userId = :user_id", Task.class)
//                .setParameter("user_id",userId)
//                .getResultList();
//    }
//
//    @Override
//    public void removeByUserId(String userId, String uuid) throws SQLException {
//        entityManager.createQuery("DELETE FROM Project p WHERE p.user_id = :user_Id AND p.id = : id", Task.class).
//                setParameter("user_id",userId)
//                .setParameter("id", uuid)
//                .executeUpdate();
//    }
//
//    @Override
//    public void removeAllByUserId(String userId) throws SQLException {
//        @NotNull final User user = entityManager.find(User.class, userId);
//        entityManager.createQuery("Delete from Task t where t.user = :user")
//                .setParameter("user", user)
//                .executeUpdate();
//    }
//
//    @Override
//    public Collection<Task> findAllByProjectId(@NotNull String userId, @Nullable String projectId) throws SQLException {
//        return entityManager.createQuery(
//                "SELECT p FROM Task p WHERE p.userId = :user_id AND p.idProject = : projectId", Task.class).
//                setParameter("user_id",userId)
//                .setParameter("idProject", projectId)
//                .getResultList();
//    }
//
//    @Override
//    public void removeAllByProjectId(String userId, @Nullable String projectId) throws SQLException {
//        entityManager.createQuery("DELETE FROM Project p WHERE p.userId = :user_id AND p.idProject = : projectId", Task.class).
//                setParameter("user_id",userId)
//                .setParameter("projectId", projectId)
//                .executeUpdate();
//    }
//}
