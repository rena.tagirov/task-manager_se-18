package ru.tagirov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.tagirov.tm.entity.Task;

import java.util.List;

@Repository
public interface TaskRepository extends JpaRepository<Task, String> {

    @NotNull
    List<Task> findAllByUser_Id(@NotNull final String userId);

    List<Task> findAllByProject_IdAndUser_Id(@NotNull String projectId, @NotNull String userId);
}
