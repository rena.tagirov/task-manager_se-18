package ru.tagirov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tagirov.tm.entity.Task;

import java.util.List;


public interface TaskService {

    //    CRUD ----------------------------------------------------------------------

    Task save(@NotNull final Task task);

    @Nullable
    List<Task> findAll();

    @Nullable Task getOne(@NotNull final String taskId);

    @Nullable
    void delete(@NotNull final Task task);

    void deleteAll();

    //    ALL ------------------------------------------------------------------------

    @NotNull
    List<Task> findAllByUser_Id(@NotNull final String userId);

    List<Task> findAllByProject_IdAndUser_Id(@NotNull String projectId, @NotNull String userId);

}
