package ru.tagirov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tagirov.tm.api.repository.ProjectRepository;
import ru.tagirov.tm.api.repository.TaskRepository;
import ru.tagirov.tm.api.repository.UserRepository;
import ru.tagirov.tm.api.service.TaskService;
import ru.tagirov.tm.entity.Task;

import java.util.List;

@Service
@Transactional
public class TaskServiceImpl implements TaskService {

    @Autowired
    ProjectRepository projectRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    TaskRepository taskRepository;

    //    CRUD ----------------------------------------------------------------------

    @Override
    public Task save(@NotNull Task task){
        return taskRepository.save(task);
    }

    @Override
    public @NotNull List<Task> findAll(){
        return taskRepository.findAll();
    }

    @Override
    public @Nullable Task getOne(@NotNull final String taskId){
        return taskRepository.getOne(taskId);
    }

    @Override
    public void delete(@NotNull final Task task){
        taskRepository.delete(task);
    }

    @Override
    public void deleteAll(){
        taskRepository.deleteAll();
    }

    //    ALL ------------------------------------------------------------------------

    @Override
    public @NotNull List<Task> findAllByUser_Id(@NotNull String userId){
        return taskRepository.findAllByUser_Id(userId);
    }

    @Override
    public List<Task> findAllByProject_IdAndUser_Id(@NotNull String projectId, @NotNull String userId){
        return taskRepository.findAllByProject_IdAndUser_Id(projectId,userId);
    }
}
