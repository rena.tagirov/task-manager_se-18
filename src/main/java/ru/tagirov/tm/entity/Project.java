package ru.tagirov.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.jetbrains.annotations.NotNull;
import ru.tagirov.tm.enumeration.Status;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "app_project")
public class Project extends PurposeEntity implements Serializable {

    private static final long serialVersionUID = -1243348624045946182L;

    @JsonIgnore
    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToMany(mappedBy = "project", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, orphanRemoval = true)
    private List<Task> tasks = new ArrayList<>();

    public Project(@NonNull final String id,
                   @NonNull final String name,
                   @NonNull final String description,
                   @NonNull final Date dateBegin,
                   @NonNull final Date dateEnd,
                   @NonNull final User user,
                   @NotNull final Status status){
        super(id, name, description, dateBegin, dateEnd, user, status);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder()
                .append("Name: ").append(getName())
                .append("\nDescription: ").append(getDescription())
                .append("\nStart date: ").append(getDateBegin())
                .append("\nEnd date: ").append(getDateEnd())
                .append("\nUser name: ").append(getUser().getLogin())
                .append("\n--------------------------------------------------");
        return builder.toString();
    }
}
