package ru.tagirov.tm.enumeration;

import org.jetbrains.annotations.NotNull;
import ru.tagirov.tm.comparator.DateBeginComparator;
import ru.tagirov.tm.comparator.DateEndComparator;
import ru.tagirov.tm.comparator.StatusComparator;
import ru.tagirov.tm.entity.PurposeEntity;

import java.util.Comparator;

public enum  PurposeComparator {
    DATE_BEGIN_COMPARATOR(new DateBeginComparator()),
    DATE_END_COMPARATOR(new DateEndComparator()),
    STATUS_COMPARATOR(new StatusComparator());

    private @NotNull Comparator<PurposeEntity> comparator;

    PurposeComparator(@NotNull final Comparator<PurposeEntity> comparator) {
        this.comparator = comparator;
    }

    @NotNull
    public Comparator<PurposeEntity> getComparator() {
        return comparator;
    }
}
