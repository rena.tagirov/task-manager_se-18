package ru.tagirov.tm.controller;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.tagirov.tm.api.service.TaskService;
import ru.tagirov.tm.dto.TaskDto;
import ru.tagirov.tm.entity.Task;
import ru.tagirov.tm.enumeration.PurposeComparator;

import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/task", produces =  MediaType.APPLICATION_JSON_VALUE)
public class TaskController {

    @Autowired
    private TaskService taskService;

    @Autowired
    private ModelMapper mapper;

    @GetMapping
    public ResponseEntity<List<TaskDto>> findAll() throws SQLException {
        List<Task> task = taskService.findAll();
        List<TaskDto> taskDto = task.stream().map(e -> mapper.map(e, TaskDto.class)).collect(Collectors.toList());
        return new ResponseEntity<>(taskDto, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<TaskDto> save(@RequestBody TaskDto taskDto) throws SQLException {
        Task task = mapper.map(taskDto, Task.class);
        Task task1 = taskService.save(task);
        TaskDto taskDto1 = mapper.map(task1, TaskDto.class);
        return new ResponseEntity<>(taskDto1, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<TaskDto> update(@RequestBody TaskDto taskDto) throws SQLException {
        Task task = mapper.map(taskDto, Task.class);
        Task task1 = taskService.save(task);
        TaskDto taskDto1 = mapper.map(task1, TaskDto.class);
        return new ResponseEntity<>(taskDto1, HttpStatus.OK);
    }

    @DeleteMapping
    public ResponseEntity<TaskDto> deleteAll() throws SQLException {
        taskService.deleteAll();
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<TaskDto> deleteById(@PathVariable("id") String id) throws SQLException {
        Task task = taskService.getOne(id);
        taskService.delete(task);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<TaskDto> getOne(@PathVariable("id") String id) throws SQLException {
        Task task = taskService.getOne(id);
        TaskDto taskDtos = mapper.map(task, TaskDto.class);
        return new ResponseEntity<>(taskDtos, HttpStatus.OK);
    }

    @GetMapping("/findAllByUserId/{id}")
    public ResponseEntity<List<TaskDto>> findAllByUserId(@PathVariable("id") String id) throws SQLException {
        List<Task> list = taskService.findAllByUser_Id(id);
        List<TaskDto> listDto = list.stream().map(e -> mapper.map(e, TaskDto.class)).collect(Collectors.toList());
        return new ResponseEntity<>(listDto, HttpStatus.OK);
    }

    @GetMapping("/findAllByProjectIdAndUserId/{projectId}/{userId}")
    public ResponseEntity<List<TaskDto>> findAllByProjectIdAndUserId(@PathVariable("projectId") String projectId,
                                                     @PathVariable("userId") String userId) throws SQLException {
        List<Task> list = taskService.findAllByProject_IdAndUser_Id(projectId, userId);
        List<TaskDto> listDto = list.stream().map(e -> mapper.map(e, TaskDto.class)).collect(Collectors.toList());
        return new ResponseEntity<>(listDto, HttpStatus.OK);
    }

    @PostMapping("/setTaskList")
    public ResponseEntity<List<TaskDto>> setProjectList(@RequestBody List<TaskDto> listDto) throws SQLException {
        List<Task> list = listDto.stream().map(e -> mapper.map(e, Task.class)).collect(Collectors.toList());
        List<TaskDto> listDto1 = list.stream().map(e -> mapper.map(e, TaskDto.class)).collect(Collectors.toList());
        return new ResponseEntity<>(listDto1, HttpStatus.OK);
    }

    @GetMapping("/findAllBySort/{sort}")
    public ResponseEntity<List<TaskDto>> findAllBySort(@PathVariable("sort") String sort) throws SQLException {
        List<Task> list = taskService.findAll();
        switch (sort) {
            case "1":
                list.sort(PurposeComparator.DATE_BEGIN_COMPARATOR.getComparator());
                break;
            case "2":
                list.sort(PurposeComparator.DATE_END_COMPARATOR.getComparator());
                break;
            case "3":
                list.sort(PurposeComparator.STATUS_COMPARATOR.getComparator());
                break;
            default:
                System.out.println("Insertion order!");
        }
        List<TaskDto> listDto1 = list.stream().map(e -> mapper.map(e, TaskDto.class)).collect(Collectors.toList());
        return new ResponseEntity<>(listDto1, HttpStatus.OK);
    }

}
