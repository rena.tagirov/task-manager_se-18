package ru.tagirov.tm.controller;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.tagirov.tm.api.service.UserService;
import ru.tagirov.tm.dto.UserDto;
import ru.tagirov.tm.entity.User;

import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/user", produces =  MediaType.APPLICATION_JSON_VALUE)
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private ModelMapper mapper;

    @GetMapping
    public ResponseEntity<List<UserDto>> findAll() throws SQLException {
        List<User> user = userService.findAll();
        List<UserDto> userDto = user.stream().map(e -> mapper.map(e, UserDto.class)).collect(Collectors.toList());
        return new ResponseEntity<>(userDto, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<UserDto> save(@RequestBody UserDto userDto) throws SQLException {
        User user = mapper.map(userDto, User.class);
        User user1 = userService.save(user);
        UserDto userDto1 = mapper.map(user1, UserDto.class);
        return new ResponseEntity<>(userDto1, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<UserDto> update(@RequestBody UserDto userDto) throws SQLException {
        User user = mapper.map(userDto, User.class);
        User user1 = userService.save(user);
        UserDto userDto1 = mapper.map(user1, UserDto.class);
        return new ResponseEntity<>(userDto1, HttpStatus.OK);
    }

    @DeleteMapping
    public ResponseEntity<UserDto> deleteAll() throws SQLException {
        userService.deleteAll();
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<UserDto> deleteById(@PathVariable("id") String id) throws SQLException {
        User user = userService.getOne(id);
        userService.delete(user);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/{login}")
    public ResponseEntity<UserDto> findByLogin(@PathVariable("login") String login) throws SQLException {
        User user = userService.findByLogin(login);
        UserDto userDto = mapper.map(user, UserDto.class);
        return new ResponseEntity<>(userDto, HttpStatus.OK);
    }

    @PostMapping("/setUserList")
    public ResponseEntity<List<UserDto>> setUserList(@RequestBody List<UserDto> listDto) throws SQLException {
        List<User> userDto = listDto.stream().map(e -> mapper.map(e, User.class)).collect(Collectors.toList());
        List<UserDto> userDto1 = userDto.stream().map(e -> mapper.map(e, UserDto.class)).collect(Collectors.toList());
        return new ResponseEntity<>(userDto1, HttpStatus.OK);
    }
}
