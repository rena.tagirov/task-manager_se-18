**Описание проекта**

TASK MANAGER - программа, в которой можно создавать свои проекты и задачи зарегистрировавшись под обычным пользователем или администратором.
Во время регистрации ваш пароль будет закодирован по MD5 алгоритму.
При просмотре проекта или задачи - будет выведено название, описание, время и дата создания и изменения (если был изменен).
Каждому проекту и задаче присвоено свой уникальный UUID код.

---

**Требования к SOFTWARE**
- MacOs
- Windows
- Linux
---
**Стек технологий**
- IntelliJ IDEA Ultimate
- Java SE Development 8
- Apache Maven version 4.0.0
- Git
- UUID
- MANIFEST.MF
- MVC(repository, service)
- MD5
- ServiceLocator
- Анотации JetBrains
- Библиотека Lombok
- Enumeration
- Сериализация\Десериализация
- JAX-B, JSON, XML, FASTERXML
- PostgreSQL
- DBeaver
- JDBC
- MyBatis (Annotations)
- JPA + Hibernate
- Weld CDI
- Spring Core
- Spring Data JPA
- Spring MVC
- Spring Boot
---

**Разработчик**

Тагиров Ренат Айратович <br>
[rena.tagirov@gmail.com](mailto:rena.tagirov@gmail.com)
---

**Для сборки приложения выполнить команду:**
```
mvn install
```
---
**Для запуска программы в консоли выполнить команду:**
```
java -jar task-manager-1.0.0.jar
```
